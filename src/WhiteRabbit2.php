<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        // Initialize the result array
        $result = array(
                '1'   => 0,
                '2'   => 0,
                '5'   => 0,
                '10'  => 0,
                '20'  => 0,
                '50'  => 0,
                '100' => 0
            );
        
        // No result If the amount is negative or equal 0
        if($amount > 0){
            $coin_arr = array_reverse(array_keys($result));
            
            // Always check the coin with the highest value first
            foreach ($coin_arr as $coin) {
                $thisCoint = intval($coin);
				
                if ($amount >= $thisCoint) {
                    // How many coin of this value is needed
                    $r = floor($amount / $thisCoint);
                    
                    // Assign to the result
                    $result[$coin] = $r;
                    
                    // Calculate the rest amount
                    $amount = $amount - $r * $thisCoint;
                }
            }
        }
		
		return $result;
    }
}