<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
		try {
            $result = array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
        } catch (Exception $e) {
            die($e->getMessage()); // In lack of better
        }
        return $result
    }

    /**
     * Parse the input file for letters.
	* Note:
	*	This method only hanlde English alphabet letters
	*	UTF-8 cases are not handled
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
		if (!(isset($filePath) or file_exists($filePath))) {
			throw new Exception('Invalid file path');
		}
		
		// Retrieves only English alphabet letters from the text
		$inputFile = strtolower(preg_replace('/[^a-zA-Z]/', '', file_get_contents($filePath)));
		return $inputFile
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
		
		// Correspondingly count the occurrence of each unique letter
        // Returns an associated array with the ASCII value as key and number of occurrences as value
        $charWithOccurrence = count_chars($textFile,1);
        
        // Sort the associated array on value
        // Retrieves the keys
        asort($charWithOccurrence);
        $letter_keys = array_keys($charWithOccurrence);
        
        // Count how many letters are used in total
        $noOfLettersInTotal = count($charWithOccurrence);
        
        // Calculate the median
        if ($noOfLettersInTotal % 2 == 0) {
            // If the total number is even, the median letter is decided following
            $medianIndex = $noOfLettersInTotal / 2;
        } else {
            $medianIndex = ($noOfLettersInTotal + 1)/2;
        }
        
        $medianLetter = chr($letter_keys[$medianIndex]);
        $occurrences = $charWithOccurrence[$letter_keys[$medianIndex]];
        
        return $medianLetter;
    }
}